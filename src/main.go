package main

import (
	"github.com/gocql/gocql"
	"time"
)

func main() {
	// init cassandra session
	initSession()

	// signatures
	sig1 := Signature{
		Signer:    "eranga",
		Comment:   "valid doc",
		Timestamp: (time.Now().UnixNano() / int64(time.Millisecond)),
	}
	sig2 := Signature{
		Signer:    "herath",
		Comment:   "invalid doc",
		Timestamp: (time.Now().UnixNano() / int64(time.Millisecond)),
	}
	sig3 := Signature{
		Signer:    "lamabda",
		Comment:   "approved doc",
		Timestamp: (time.Now().UnixNano() / int64(time.Millisecond)),
	}

	// documents
	doc1 := &Document{
		Company:    "rahasak",
		Id:         gocql.TimeUUID(),
		Name:       "invoice-01",
		Tags:       []string{"inv", "ops"},
		Signatures: []Signature{sig1, sig2},
		Status:     "pending",
		Timestamp:  time.Now(),
	}
	doc2 := &Document{
		Company:    "rahasak",
		Id:         gocql.TimeUUID(),
		Name:       "order-02",
		Tags:       []string{"ord", "ops"},
		Signatures: []Signature{sig1, sig2},
		Status:     "pending",
		Timestamp:  time.Now(),
	}

	// create doc
	createDocument(doc1)
	createDocument(doc2)

	// query document
	getDocument("rahasak", doc1.Id)

	// update document
	updateDocument(doc1.Company, doc1.Id, "order-01", "approved")

	// add signature
	addSignature("rahasak", doc1.Id, sig3)

	// remove singature
	removeSignature("rahasak", doc1.Id, sig2)

	// add tag
	addTag("rahasak", doc1.Id, "invoice")

	// remove tag
	removeTag("rahasak", doc1.Id, "ops")

	// clear session
	clearSession()
}

package main

import (
	"github.com/gocql/gocql"
	"time"
)

func timestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func uuid() gocql.UUID {
	return gocql.TimeUUID()
}

func touuid(cid string) (gocql.UUID, error) {
	return gocql.ParseUUID(cid)
}

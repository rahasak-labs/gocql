package main

import (
	"os"
)

type CassandraConfig struct {
	host        string
	port        string
	keyspace    string
	consistancy string
}

var cassandraConfig = CassandraConfig{
	host:        getEnv("CASSANDRA_HOST", "dev.localhost"),
	port:        getEnv("CASSANDRA_PORT", "9042"),
	keyspace:    getEnv("CASSANDRA_KEYSPACE", "mystiko"),
	consistancy: getEnv("CASSANDRA_CONSISTANCY", "LOCAL_QUORUM"),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

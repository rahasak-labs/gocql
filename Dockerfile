FROM golang:1.9

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install dependencies
RUN go get github.com/gocql/gocql
RUN go get github.com/mitchellh/mapstructure

# env
ENV CASSANDRA_HOST dev.localhost
ENV CASSANDRA_PORT 9042
ENV CASSANDRA_KEYSPACE zchain

# copy app
ADD . /app
WORKDIR /app

# build
RUN go build -o build/gocql src/*.go

ENTRYPOINT ["/app/docker-entrypoint.sh"]
